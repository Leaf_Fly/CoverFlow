using UnityEngine;
using System.Collections;

/* Computes a fading between 2 given colors */

public class UIEffectColor : UIEffectTemplate {
	
	private Color m_oColorIn;
	private Color m_oColorOut;
	private Color m_oDistanceBetweenColors;
	private Material m_rCachedMaterial;
	private string m_rColorName;

	public Color colorIn
	{
		get
		{
			return m_oColorIn;
		}
		set
		{
			m_oColorIn = value;
			m_oDistanceBetweenColors = m_oColorOut - m_oColorIn;
		}
	}

	public Color colorOut
	{
		get
		{
			return m_oColorOut;
		}
		set
		{
			m_oColorOut = value;
			m_oDistanceBetweenColors = m_oColorOut - m_oColorIn;
		}		
	}

	public Color currentColor
	{
		get
		{
			Color oComputedFadingColor = Color.black;
	
			// Computes fading
			oComputedFadingColor.r = m_dEasingFunction( m_oColorIn.r, m_oDistanceBetweenColors.r, m_fTimer, m_fDuration );
			oComputedFadingColor.g = m_dEasingFunction( m_oColorIn.g, m_oDistanceBetweenColors.g, m_fTimer, m_fDuration );
			oComputedFadingColor.b = m_dEasingFunction( m_oColorIn.b, m_oDistanceBetweenColors.b, m_fTimer, m_fDuration );
			oComputedFadingColor.a = m_dEasingFunction( m_oColorIn.a, m_oDistanceBetweenColors.a, m_fTimer, m_fDuration );

			return oComputedFadingColor;
		}
	}

	public string colorName
	{
		set
		{
			m_rColorName = value;
		}
		get
		{
			return m_rColorName;
		}
	}

	protected override void InternalInit( )
	{
		base.InternalInit( );
		m_rCachedMaterial = renderer.material;
		m_rColorName = "_Color";

		m_oColorIn  = m_rCachedMaterial.GetColor( m_rColorName );
		m_oColorOut = m_oColorIn;

		m_oDistanceBetweenColors = Color.black; // m_oColorOut = m_oColorIn => m_oColorOut - m_oColorIn = 0 => Color.black;	
	}

	protected override void EffectUpdate( )
	{
		Color oComputedFadingColor = Color.black;

		// Computes fading
		oComputedFadingColor.r = m_dEasingFunction( m_oColorIn.r, m_oDistanceBetweenColors.r, m_fTimer, m_fDuration );
		oComputedFadingColor.g = m_dEasingFunction( m_oColorIn.g, m_oDistanceBetweenColors.g, m_fTimer, m_fDuration );
		oComputedFadingColor.b = m_dEasingFunction( m_oColorIn.b, m_oDistanceBetweenColors.b, m_fTimer, m_fDuration );
		oComputedFadingColor.a = m_dEasingFunction( m_oColorIn.a, m_oDistanceBetweenColors.a, m_fTimer, m_fDuration );

		// Sets faded color
		m_rCachedMaterial.SetColor( m_rColorName, oComputedFadingColor );
	}
}
