using UnityEngine;
using System.Collections;

/* TabGroup is a class which manages a group of tabs.
 * The managed tabs are the children of the manager.
 * When selected, a tab sends a OnTabButtonClickedEvent message to the tab group manager.
 * In return, the manager sends OnDeselectedEvent and OnSelectedEvent messages
 * to the previous selected tab and the new selected tab, respectively.
 * 
 * Of course, this behaviour may be overrided.
 */
public class TabButtonGroupUniflowGalleryControl : MonoBehaviour
{
	public TabButtonUniflowGalleryControl activeTabButton;

	void Start( )
	{
		if( activeTabButton != null )
		{
			activeTabButton.OnTabButtonSelectedEvent( );
		}
	}

	private void OnTabButtonClickedEvent( TabButtonUniflowGalleryControl a_rSelectedTabButton )
	{
		if( activeTabButton != a_rSelectedTabButton )
		{
			if( activeTabButton != null )
			{
				activeTabButton.OnTabButtonDeselectedEvent( );
			}

			activeTabButton = a_rSelectedTabButton;
			activeTabButton.OnTabButtonSelectedEvent( );
		}
	}
}
